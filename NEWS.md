# 0.4.0

Unreleased.

# 0.3.0

Released on the 8th June 2019.

## Platform support

* C++14 is now the minimum required language version, with C++17 being
  used optionally when available.
* Microsoft Visual Studio 2017 and 2019 are now both supported.
* LLVM 6 is now supported.
* GCC 9 is now supported in C++14 mode; C++17 is currently unsupported
  since this release changed the type of
  `std::filesystem::file_time_type` returned by
  `std::filesystem::last_write_time()`, which requires non-portable
  conversion to `std::chrono::system_clock::time_point`.  This will be
  worked around in a future release, or with C++20 which will
  introduce portable time point conversion functions.

Source changes:

* Boost library removal (!5).
* Xerces and Xalan dependencies can be optionally replaced (!7).
* Use MPark.Variant (!1).
* Use 4D pixel buffer (!2).
* Update CMake dependency versions for OME Compat, OME Common, OME XML
  and OME Files (!11).
* Update maven dependency version for formats-bsd and formats-gpl
  (!11).

## Infrastructure changes

* Continuous integration testing is performed on GitLab, testing on
  FreeBSD, Linux, MacOS X and Windows platforms.
  (!3, !6, !9, !10).
